//  Task 1 
function vowelsFinder(inputText) {
  const counter = inputText.match(/[aeiou]/gi).length;
  return counter;
}
const string = prompt("Enter a sentence to find vowels: ");
const found = vowelsFinder(string);
console.log(found);

//  Task 2 

// One day  =  24 * 60 * 60 * 1000 = days * 86400000 ms

let currentday = new Date();
let newYear = new Date(2022, 01, 01);

let calcDiffer = newYear.getTime() - currentday.getTime();
let msInDay = 1000 * 60 * 60 * 24;

let result = Math.floor(calcDiffer / msInDay);
console.log(
  `When you sleep over ${result} days, you will get a gift from Santa )) `
);

// Task 3
function symbolFinder(array) {
  const symbolsArray = ["!", "@", "#", "$", "%", "^", "&", "*", "?"];
  let countSymbol = [];
  symbolsArray.forEach((elem) => {
    array.forEach((symbol) => {
      if (elem === symbol) {
        countSymbol.push(symbol);
      }
    });
  });
  console.log(countSymbol);
}
symbolFinder(["symbol", "#", 23, "!", "@", 32, "finder", "%"]);
// symbolFinder([prompt("Enter a symbol: ")]);

// Task 4 
let array1 = [1, 2, 3, 4, 5];
let array2 = [3, 4, 5, 6, 7];

const filteredArray = array1.filter((value) => array2.includes(value));
console.log(filteredArray);

// Task 5 
function myFunction() {
  setTimeout(function () {
    alert("Thanks for waiting me 4 secund");
  }, 4000);
}

// Task 6

function adderNatural(number) {
  let sum = 0;
  for (let i = 1; i <= number; i++) {
    sum = sum + i;
  }
  console.log(sum);
}
adderNatural(9);

// Task 7 

charcounter(prompt("Enter a sentence to count character"));

function charcounter(str) {
  let counted = str.length;
  console.log(counted);
}
